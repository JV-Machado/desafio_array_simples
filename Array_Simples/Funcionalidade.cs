﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Array_Simples
{
    class Funcionalidade
    {
        public string[] Array { get; set; }
        public string[] ArrayCopy { get; set; }
        public string Titulo { get; set; }
        public int Tamanho { get; set; }
        public int T { get; set; }

        public Funcionalidade()
        {
            T = 0;
            Array = new string[T];

            Tamanho = 0;
        }

        public void AdicionarCotacao(string nome)
        {      
            Titulo = nome;
            Array = Push(Array,Titulo);

            Tamanho++;
        }

        public void RemoverCotacao(int pos)
        {
            Array = Delete(Array, pos);
            Tamanho--;
        }

        public void AlterarTituloCotacao(int pos, string nome)
        {
            Titulo = nome;
            Array[pos] = Titulo;
        }

        public string[] Push(string[] v, string b)
        {
            ArrayCopy = new string[v.Length+1];
            v.CopyTo(ArrayCopy, 0);
            ArrayCopy[ArrayCopy.Length - 1] = b;
            v = ArrayCopy;
            return v;
        }

        public string[] Delete(string[] v, int pos)
        {
            int j = 0;
            ArrayCopy = new string[v.Length - 1];
            for(int i = 0; i < v.Length; i++)
            {
                if(i != pos)
                {
                    ArrayCopy[j] = v[i];
                    j++;
                }
            }
            v = ArrayCopy;
            return v;
        }

        public void Interacao()
        {
            Console.WriteLine("Digite: 1/2/3/-1");
            Console.WriteLine("1 - Para adicionar cotação");
            Console.WriteLine("2 - Para remover cotação");
            Console.WriteLine("3 - Para alterar nome da cotação");
            Console.WriteLine("-1 - Para encerrar alterações");

            while (true)
            {
                Console.WriteLine();
                Console.Write("Comando: ");
                int n = int.Parse(Console.ReadLine());
                Console.WriteLine();
                if (n == 1)
                {
                    Console.Write("Digite o nome da cotação que deseja adicionar: ");
                    string b = Console.ReadLine();
                    AdicionarCotacao(b);
                }
                else if (n == 2)
                {
                    if (Tamanho == 0)
                    {
                        Console.WriteLine("Vazio! Adicione primeiro");
                        Console.Write("Digite 1 para adicionar: ");
                        Console.WriteLine();
                    }
                    else
                    {
                        Console.Write("Digite em que posição deseja remover: ");
                        int a = int.Parse(Console.ReadLine());
                        if (a > Tamanho-1)
                        {
                            Console.WriteLine("Posição inválida!");
                        }
                        else
                        {
                            RemoverCotacao(a);
                        }

                    }

                }
                else if (n == 3)
                {
                    if (Tamanho == 0)
                    {
                        Console.WriteLine("Vazio! Adicione primeiro");
                        Console.Write("Digite 1 para adicionar: ");
                        Console.WriteLine();
                    }
                    else
                    {
                        Console.Write("Digite em que posição que deseja alterar: ");
                        int a = int.Parse(Console.ReadLine());
                        if (a > Tamanho-1)
                        {
                            Console.WriteLine("Posição inválida!");
                        }
                        else
                        {
                            Console.Write("Digite o nome da cotação que deseja alterar: ");
                            string b = Console.ReadLine();
                            AlterarTituloCotacao(a, b);
                        }

                    }
                }
                else if (n == -1)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Digite um comando válido!");
                }
            }
        }

        public void ImprimirArray()
        {
            for (int i = 0; i < Array.Length; i++)
            {
                Console.WriteLine($"{i}: {Array[i]}");
            }
        }

    }
}
